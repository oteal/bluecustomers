﻿using MongoDB.Bson.Serialization.Attributes;

namespace Blue.Models {
	public class Customer {
		[BsonId]
		public int Id { get; set; }
		[BsonElement("name")]
		public string Name { get; set; }
		[BsonElement("surname")]
		public string Surname { get; set; }
		[BsonElement("email")]
		public string Email { get; set; }
		[BsonElement("password")]
		public string Password { get; set; }
	}
}