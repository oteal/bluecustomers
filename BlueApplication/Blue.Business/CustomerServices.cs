﻿using Blue.DAL;
using Blue.Models;

namespace Blue.Business {
	public class CustomerServices {
		ICustomerRepository customerPersistence;
		public CustomerServices(ICustomerRepository personRepository) {
			customerPersistence = personRepository;
		}

		public async Task<Customer> GetCustomer(int id) {
			return await customerPersistence.Get(id);
		}

		public async Task<bool> AddCustomer(Customer customer) {
			return await customerPersistence.Insert(customer);
		}

		public async Task<bool> UpdateCustomer(int id, Customer customer) {
			if (await ExistsCustomer(id)) {
				return await customerPersistence.Update(id, customer);
			}
			else {
				return false;
			}
		}

		public async Task<bool> DeleteCustomer(int id) {
			if (await ExistsCustomer(id)) {
				return await customerPersistence.Delete(id);
			}
			else {
				return false;
			}
		}

		public async Task<bool> ExistsCustomer(int customerId) {
			return await customerPersistence.Get(customerId) != null;
		}

		public async Task<List<Customer>> GetAllCustomers() {
			return await customerPersistence.GetAll();
		}
	}
}