﻿using Blue.Business;
using Blue.DAL;
using Blue.Models;
using MongoDB.Driver;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blue.Tests {
  [TestClass]
  public class CustomerServicesUnitTest {
    private Mock<IMongoDatabase> mongodb;
    private Mock<IMongoCollection<Customer>> productCollection;
    public CustomerServicesUnitTest() {
      this.productCollection = new Mock<IMongoCollection<Customer>>();
      this.mongodb = new Mock<IMongoDatabase>();
    }

    private void InitializeMongoDb() {
      this.mongodb.Setup(x => x.GetCollection<Customer>("BlueCustomers",
          default)).Returns(this.productCollection.Object);
    }
    private void InitializeMongoProductCollection() {
      this.InitializeMongoDb();
    }

    [TestMethod]
    public void Add1() {
      this.InitializeMongoProductCollection();

      var service = new CustomerServices(new CustomerRepository(mongodb.Object));

      var added = service.AddCustomer(new Customer {
        Id = 1,
        Name = "João",
        Surname = "Santos",
        Email = "joao.santos89@outlook.com",
        Password = "P@$$w0rd!"
      }).Result;

      Assert.IsTrue(added);

    }

    [TestMethod]
    public void Update1() {
      this.InitializeMongoProductCollection();
      var service = new CustomerServices(new CustomerRepository(mongodb.Object));

      var updated = service.UpdateCustomer(2, new Customer { Id = 1, Name = "John", Surname = "Saints" }).Result;
      Assert.IsTrue(updated);
    }

    [TestMethod]
    public void Delete1() {
      this.InitializeMongoProductCollection();
      var service = new CustomerServices(new CustomerRepository(mongodb.Object));

      var deleted = service.DeleteCustomer(1).Result;
      Assert.IsTrue(deleted);
    }
  }
}
