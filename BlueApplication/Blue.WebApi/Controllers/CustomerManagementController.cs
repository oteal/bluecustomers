using Blue.Business;
using Blue.DAL;
using Blue.Models;
using Microsoft.AspNetCore.Mvc;

namespace Blue.WebApi.Controllers {
	[ApiController]
	[Route("[controller]")]
	public class CustomerManagementController : ControllerBase {

		private readonly CustomerServices services;

		private readonly ILogger<CustomerManagementController> _logger;

		public CustomerManagementController(ILogger<CustomerManagementController> logger, ICustomerRepository personRepository) {
			_logger = logger;
			services = new CustomerServices(personRepository);
		}

		[HttpGet("GetAll")]
		public async Task<List<Customer>> GetAll() {
			return await services.GetAllCustomers();
		}

		[HttpGet("Get")]
		public async Task<Customer> Get(int id) {
			return await services.GetCustomer(id);
		}

		[HttpPost("Insert")]
		public async Task<bool> Insert(Customer newCustomer) {
			return await services.AddCustomer(newCustomer);
		}

		[HttpPut("Update")]
		public async Task<bool> Update(int id, Customer customer) {
			return await services.UpdateCustomer(id, customer);
		}

		[HttpDelete("Delete")]
		public async Task<bool> Delete(int id) {
			return await services.DeleteCustomer(id);
		}

	}
}