﻿using Blue.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blue.DAL {
	public interface ICustomerRepository {
		Task<List<Customer>> GetAll();
		Task<Customer> Get(int id);
		Task<bool> Insert(Customer customer);
		Task<bool> Update(int id, Customer customer);
		Task<bool> Delete(int id);
	}
}
