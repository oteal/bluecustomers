﻿using Blue.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blue.DAL {
	public class CustomerRepository : ICustomerRepository {

    private readonly IMongoCollection<Customer> _customerCollection;

    public CustomerRepository(IMongoDatabase mongoDatabase) {
      _customerCollection = mongoDatabase.GetCollection<Customer>("BlueCustomers");
    }

		public async Task<List<Customer>> GetAll() {
			return await _customerCollection.Find(m => true).ToListAsync();
		}
		public async Task<Customer> Get(int id) {
			return await _customerCollection.Find(m => m.Id == id).FirstOrDefaultAsync();
		}

		public async Task<bool> Insert(Customer customer) {
			await _customerCollection.InsertOneAsync(customer);
			return true;
		}

		public async Task<bool> Update(int id, Customer customer) {
			await _customerCollection.ReplaceOneAsync(p => p.Id == id, customer);
			return true;
		}

		public async Task<bool> Delete(int id) {
			await _customerCollection.DeleteOneAsync(p => p.Id == id);

			return true;
		
		}

	}
}
