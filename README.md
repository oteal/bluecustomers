# Blue App
## Global Blue test 
by João Santos | 27-07-2022 | time spent: around 4 hours

The app has 5 projects wich represents different architecture layers. The most difficulties where mocking up the database to unit testing because I never did it.

## Requirements
1. Docker installed

## How to start app
1. Open cmd prompt where Dockerfile is located
2. Build the app container:
``docker build -t joao.santos/bluewebapi .``
3. Launch the app:
``docker run -p 8080:80 joao.santos/bluewebapi``
4. Access to
``http://localhost:8080/swagger/index.html``
5. Try the methods by yourself

## Issues
1. Unit testing Get and GetAll methods don't work while using data mock 