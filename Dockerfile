FROM mcr.microsoft.com/dotnet/sdk:6.0-focal AS build-env
WORKDIR /source

COPY /BlueApplication .
RUN dotnet restore "./Blue.WebApi/Blue.WebApi.csproj"

#COPY . /app
RUN dotnet publish "./Blue.WebApi/Blue.WebApi.csproj" -o /app

FROM mcr.microsoft.com/dotnet/aspnet:6.0-focal
WORKDIR /app
COPY --from=build-env /app ./
EXPOSE 80
ENTRYPOINT [ "dotnet", "Blue.WebApi.dll" ] 